const Nanocomponent = require('nanocomponent')
const html = require('choo/html')
const css = require('sheetify')
const imgStyle = css`
:host{
  object-fit:cover;
  width:100%;
  height:400px;
}
`

const linkStyle = css`
:host {
  text-shadow:3px 2px 2px #919191;
}
`

class Project extends Nanocomponent {
  constructor(project){
    super()
    this.project = project
  }
  createElement(){
    return html`
      <div class="flex w-50 h-50 flex-column pr5">
        <h3 class="h-10">${this.project.name}</h3>
        <a class="h-25 grow" href=${this.project.link} target="_blank">
          <img class="${imgStyle} o-50 z-1" src=${this.project.image}/>
          <p class="${linkStyle} z-2 absolute bottom-1 link white ml3"> ${this.project.description}</p>
        </a>
      </div>
    `
  }
  update(){
    return true
  }
}
module.exports = Project
