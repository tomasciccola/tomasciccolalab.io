const choo = require('choo')
const app = choo()
const html = require('choo/html')
const css = require('sheetify')
css('tachyons')

const main = css`
@font-face {
  font-family: "jetbrains";
  src:url("jetbrains.ttf");
}
body {
  width:100vw;
  height:100vh;
  background:#1c1c1c;
  overflow-x:hidden;
  font-family: jetbrains;
}

@media only screen and (max-device-width: 480px){
  #app{
    width:80%;
  }
  h1 {
    font-size: 3rem!important;
  }
  h2 {
    font-size:3.0rem!important;
  }
  h3 {
    font-size:2.5rem;
  }
  p { 
    font-size:2.0rem;
  }

}

`

app.route('/', (state,emit) => {
  return html`
  <div id="app" class="${main} w-50 h-100 center tl light-gray pv6 lh-copy" >
  ${require('./views/header.js')()}
  ${require('./views/projects.js')()}
  </div>
  `
})

app.mount('#app')

