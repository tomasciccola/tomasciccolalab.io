const html = require('choo/html')

module.exports = (state,emit) => {
  return html`
  <div class="f4 w-100 tl self-start mt4">
    <p>Hi! I'm a software developer from Argentina, with 8 years of experience building fullstack apps.</p>
    <p> I also have a bachelor's degree in new media arts. </p>
    <p> I've regulary worked with different production companies from Argentina.</p>
    <p> Some of them are <a class="link green" href="https://www.instagram.com/holaplan/" >PLAN</a> and
    <a class="link green" href="https://www.lepes.com.ar">Lepes</a>.
    <p>
    I mostly use javascript with the <a target="_blank" class="link green dim b" href="https://mern.js.org/">MERN</a> stack.
    </p>
    <p>
    I sporadically use C++, Arduino, Lua, Bash and Python. 
    </p>
    <p>
    I also do stuff related to experimental tech. You can check that stuff 
    <a href="" target="_blank" class="link green dim b">here</a>.
    </p>
  </div>
  `
}
