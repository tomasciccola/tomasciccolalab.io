const html = require('choo/html')
const css = require('sheetify')
const style = css`
:host {
flex-wrap:wrap;
}
`

module.exports = (state,emit) => {
  return html`
  <div class="flex flex-row pt3 w-100 justify-around ${style}">
    <img class="br-pill w-40"
    src="profile.jpeg" 
    alt="image showing the face of Tomás Ciccola"/>
    <div class="tr">
      <h1 class="f2">Tomás Ciccola</h1>
      ${require('./links.js')()}
    </div>
  </div>
  `
}
