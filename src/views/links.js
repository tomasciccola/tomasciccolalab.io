const html = require('choo/html')

module.exports = (state,emit) => {
  return html`
    <ul class="list flex flex-column pl0 f4">
      <li class="mb2 w-100">
       <a class="green link dim" 
       href="https://gitlab.com/tomasciccola"
       target="_blank"/>
       gitlab.com/tomasciccola
       <span class="light-gray">   </span>
       </a>
      </li>
      <li class="mb2 w-100">
        <a class="green link dim" 
        href="https://www.linkedin.com/in/tomás-ciccola"
        target="_blank"/>
        linkedin.com/in/tomás-ciccola
        <span class="light-gray">  </span>
        </a>
      </li>
      <li class="w-100">
        <a class="green link dim" 
        href="mailto:tomas.ciccola@gmail.com" />
        tomas.ciccola@gmail.com
        <span class="light-gray">  </span> 
        </a>
        </li>
      <li class="w-100">
        <a class="green link dim" href="cv.pdf" download="tomasciccola.pdf" />
        CV 
        <span class="light-gray">  </span> 
        </a>
        </li>
    </ul>
    </ul>
  `
}
