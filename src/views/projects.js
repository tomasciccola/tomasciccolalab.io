const html = require('choo/html')
const Project = require('../components/Project.js')
const css = require('sheetify')

const style = css`
:host {
flex-wrap:wrap;
}
`

/*
 * Project {
 * Name
 * Description
 * Image
 * Link
 * }
 */
const projects = require('./projects.json')
module.exports = (state,emit) => {
  return html` 
  <h2 class="f2 bb pb2 b--green">Projects</h2>
  <div class="flex w-100 flex-row mt5 center ${style}">
    ${projects.map(pr => new Project(pr).render())} 
  </div>`
}
