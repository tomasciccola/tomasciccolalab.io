const html = require('choo/html')

module.exports = (state,emit) => {
  return html`
    <div class="flex flex-column center justify-between">
      ${require('./badge.js')()}
      ${require('./bio.js')()}
    </div>
  `
}
